package com.wei.tkmapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "com.wei.tkmapper.mapper")
public class TkmapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(TkmapperApplication.class, args);
		//TODO
		//tuxianchao modify
	}
}
