package com.wei.tkmapper.mapper;

import com.wei.tkmapper.pojo.User;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.CacheNamespaceRef;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@CacheNamespaceRef(UserMapper.class)
public interface UserMapper extends Mapper<User> {
}