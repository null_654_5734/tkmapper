package com.wei.tkmapper.controller;

import cn.hutool.core.lang.Console;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.wei.tkmapper.mapper.UserMapper;
import com.wei.tkmapper.pojo.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author : wangwei
 * Data     : 2018/4/15
 * Time     : 16:35
 */
@RestController
public class TestMapperController {

    //推荐创建不可变静态类成员变量
    private static final Log log = LogFactory.get();

    @Resource
    private UserMapper userMapper;

    @GetMapping("/hello")
    public List<User> hello(){
        List<User> users = userMapper.selectAll();
        return users;
    }

}
